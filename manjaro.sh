#!/bin/bash
#https://bit.ly/322yiuj
clear
while :; do
    echo ""
    echo ""
    echo "THIS SCRIPT IS FOR ARCH-BASED DISTRIBUTIONS."
    echo "Please don't try to run this on Ubuntu."
    echo ""
    echo "Please select an option"
    echo "1)  Remove unwanted applications"
    echo "2)  Automated package install"
    echo "3)  Manual package install"
    echo "4)  Perform options 1 & 2"
    echo "99) Exit"
    echo ""
    read INPUT_STRING
    case $INPUT_STRING in

    1)
        #Removing unwanted applications
        sudo pacman -Syu --noconfirm
        sudo pacman -Rs --noconfirm midori
        sudo pacman -Rs --noconfirm parole
        sudo pacman -Rs --noconfirm qpdfview

        echo ""
        echo "Task Completed!"
        echo "Returning to menu. . ."
        ;;

    2)
        #Latest Java
        sudo pacman -S --noconfirm jre-openjdk

        #VLC
        sudo pacman -S --noconfirm vlc

        #Signal Desktop
        sudo pacman -S --noconfirm signal-desktop

        #Discord
        sudo pacman -S --noconfirm discord

        #Steam
        sudo pacman -S --noconfirm steam

        #FileZilla
        sudo pacman -S --noconfirm filezilla

        #Yay
        sudo pacman -S --noconfirm base-devel
        sudo pacman -S --noconfirm git
        cd ~
        git clone https://aur.archlinux.org/yay.git
        cd yay
        makepkg -si --noconfirm
        cd ~

        #VSCodium
        yay -S --noconfirm vscodium-bin

        #TeamViewerteam
        yay -S --noconfirm teamviewer

        #MultiMC 5
        yay -S --noconfirm multimc5 --noconfirm

        #Brave Dev
        yay -S --noconfirm brave-dev-bin --noconfirm

        echo ""
        echo "Task Completed!"
        echo "Returning to menu. . ."
        ;;

    3)
        #Latest Java
        sudo pacman -S jre-openjdk

        #VLC
        sudo pacman -S vlc

        #Signal Desktop
        sudo pacman -S signal-desktop

        #Discord
        sudo pacman -S discord

        #Steam
        sudo pacman -S steam

        #FileZilla
        sudo pacman -S filezilla

        #Yay
        sudo pacman -S base-devel
        sudo pacman -S git
        cd ~
        git clone https://aur.archlinux.org/yay.git
        cd yay
        makepkg -si
        cd ~

        #VSCodium
        yay -S vscodium-bin

        #TeamViewerteam
        yay -S teamviewer

        #MultiMC 5
        yay -S multimc5

        #Brave Dev
        yay -S brave-dev-bin
        ;;

    4)
        #Removing unwanted applications
        sudo pacman -Syu --noconfirm
        sudo pacman -Rs --noconfirm midori
        sudo pacman -Rs --noconfirm parole
        sudo pacman -Rs --noconfirm qpdfview
        
        #Latest Java
        sudo pacman -S --noconfirm jre-openjdk

        #VLC
        sudo pacman -S --noconfirm vlc

        #Signal Desktop
        sudo pacman -S --noconfirm signal-desktop

        #Discord
        sudo pacman -S --noconfirm discord

        #Steam
        sudo pacman -S --noconfirm steam

        #FileZilla
        sudo pacman -S --noconfirm filezilla

        #Yay
        sudo pacman -S --noconfirm base-devel
        sudo pacman -S --noconfirm git
        cd ~
        git clone https://aur.archlinux.org/yay.git
        cd yay
        makepkg -si --noconfirm
        cd ~

        #VSCodium
        yay -S --noconfirm vscodium-bin

        #TeamViewerteam
        yay -S --noconfirm teamviewer

        #MultiMC 5
        yay -S --noconfirm multimc5 --noconfirm

        #Brave Dev
        yay -S --noconfirm brave-dev-bin --noconfirm

        echo ""
        echo "Task Completed!"
        echo "There is nothing to do! Exiting. . ."
        exit
        ;;

    99)
        break
        ;;

    *)
        clear
        echo "Invalid input. Try again."
        ;;
    esac
done
