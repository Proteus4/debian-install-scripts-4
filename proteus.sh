#!/bin/bash
#https://bit.ly/2ChYjLb
while :; do
    clear
    echo ""
    echo ""
    echo "THIS SCRIPT IS FOR DEBIAN-BASED DISTRIBUTIONS."
    echo "Please don't try to run this on Arch"
    echo ""
    echo "Please select an option"
    echo "1) Remove unwanted applications"
    echo "2) Switch to a local mirror for Focal Fossa & Ulyana (Internode)"
    echo "3) Automated package install"
    echo "4) Install themes and configs"
    echo "5) Full setup"
    echo "99) Exit"

    read INPUT_STRING
    case $INPUT_STRING in

    1)
        sudo apt remove -y --purge firefox;
        sudo apt remove -y --purge firefox-locale-en;
        sudo apt remove -y --purge celluloid;
        sudo apt remove -y --purge drawing;
        sudo apt remove -y --purge xreader;
        sudo apt remove -y --purge simple-scan;
        sudo apt remove -y --purge gnote;
        sudo apt remove -y --purge hexchat;
        sudo apt remove -y --purge pix;
        sudo apt remove -y --purge rhythmbox;
        sudo apt remove -y --purge thunderbird;
        ;;
        
    2)
        sudo wget --content-disposition https://bit.ly/2DwOoC9;
        sudo cp -r official-package-repositories.list /etc/apt/sources.list.d/;
        sudo rm official-package-repositories.list;
        sudo apt update;
        echo ""
        echo "Successfully switched to a local mirror (Internode)!"
        echo ""
        sleep 5;
        ;;
    
    3)
        sudo apt update
        sudo apt upgrade -y

        #brave browser beta
        sudo apt install -y apt-transport-https;
        sudo apt install -y curl;
        curl -s https://brave-browser-apt-beta.s3.brave.com/brave-core-nightly.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-prerelease.gpg add -
        echo "deb [arch=amd64] https://brave-browser-apt-beta.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-beta.list
        sudo apt update -y;
        sudo apt install brave-browser-beta -y;
        
        #discord
        wget -O discord.deb "https://discordapp.com/api/download?platform=linux&format=deb";
        sudo dpkg -i discord.deb;
        sudo apt install -f -y;
        rm discord.deb;
        
        sudo apt install -y git;
        sudo apt install -y curl;
        curl "https://raw.githubusercontent.com/bb010g/betterdiscordctl/master/betterdiscordctl" > betterdiscordctl;
        chmod +x betterdiscordctl;
        sudo mv betterdiscordctl /usr/local/bin/;
        nohup discord >& /dev/null &

        #gmusicbrowser
        git clone https://github.com/squentin/gmusicbrowser.git;
        cd gmusicbrowser/;
        make install;
        sudo make install;
        cd ..;
        sudo rm -r gmusicbrowser/;
        
        #vlc
        sudo apt install -y vlc;

        #gnome softwarec
        sudo apt install -y gnome-software;

        #screenfetch
        sudo apt install -y screenfetch;
        
        #dislocker
        sudo apt install -y dislocker;
        
        killall firefox;
        sudo apt remove -y --purge firefox firefox-locale-en;
        betterdiscordctl install -s /usr/share;
        killall Discord;

        #ffmpeg
        sudo apt install -y ffmpeg;
        
        #resilio sync
        sudo wget -qO - https://linux-packages.resilio.com/resilio-sync/key.asc | sudo apt-key add -;
        sudo echo "deb http://linux-packages.resilio.com/resilio-sync/deb resilio-sync non-free" | sudo tee /etc/apt/sources.list.d/resilio-sync.list;
        sudo apt update -y
        sudo apt install -y resilio-sync;
        sudo service resilio-sync stop;
        sudo systemctl disable resilio-sync;
        wget https://gitlab.com/Proteus4/debian-install-scripts-4/-/raw/master/resilio-sync.service;
        sudo mv resilio-sync.service /usr/lib/systemd/user/;
        systemctl --user enable resilio-sync;
        systemctl --user start resilio-sync;
        
        #vscodium
        wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | sudo apt-key add -;
        echo 'deb https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list;
        sudo apt update -y;
        sudo apt install -y codium;

        #signal
        sudo apt install -y curl;
        curl -s https://updates.signal.org/desktop/apt/keys.asc | sudo apt-key add -;
        echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list;
        sudo apt update -y;
        sudo apt install -y signal-desktop;

        #teamviewer
        wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb;
        sudo apt install -y ./teamviewer_amd64.deb;
        rm teamviewer_amd64.deb;

        #audacity
        sudo apt install -y audacity;
        sudo apt install -y steam;
        
        sudo apt --fix-broken install -y;
        sudo apt autoremove -y;
        
        #tlp
        sudo add-apt-repository -y ppa:linrunner/tlp
        sudo apt install -y tlp;        
        
        #encfs
        sudo apt install -y encfs;

        ;;
    4)
        sudo apt update
        sudo apt upgrade -y
    
        #chicago 95
        sudo apt install -y git;
        git clone https://github.com/grassmunk/Chicago95.git;
        mkdir -p ~/.themes;
        mkdir -p ~/.icons;
        cp -r Chicago95/Theme/Chicago95 ~/.themes;
        cp -r Chicago95/Icons/* ~/.icons;
        sudo rm -r Chicago95;

        #arc-theme
        sudo apt install -y arc-theme

        #papirus icon theme
        sudo apt install -y papirus-icon-theme;

        #gmusicbrowser config
        wget --content-disposition https://bit.ly/2ZVFp4U;
        unzip -o gmusicbrowser.zip;
        cp -r gmusicbrowser ~/.config/;
        rm gmusicbrowser.zip;
        rm -r gmusicbrowser;
        
        #vlc config
        wget --content-disposition https://bit.ly/2Oe13Mf;
        unzip -o vlc.zip;
        cp -r vlc ~/.config/;
        rm -r vlc;
        rm -vlc.zip;
        
        #xfce4 config
        rm -r ~/.config/xfce4;
        wget --content-disposition https://bit.ly/2ARU8FE;
        unzip -o xfce4.zip;
        cp -r xfce4 ~/.config/;
        rm -r xfce4;
        rm xfce4.zip;
        
        #xfce4 panel config
        sudo apt install -y xfpanel-switch;
        wget --content-disposition https://bit.ly/3iQJWhM;
        xfce4-panel-profiles load mint-panel;
        rm mint-panel;

        #Matcha theme
        sudo apt install -y gtk2-engines-murrine;
        sudo apt install -y gtk2-engines-pixbuf;
        sudo apt install -y git;
        git clone https://github.com/vinceliuice/matcha.git;
        cd matcha;
        ./install.sh;
        cd ..;
        sudo rm -r matcha;
        
        #Papirus + Matcha Dark Sea config
        xfconf-query -c xfwm4 -p /general/theme -s "Matcha-dark-sea";
        xfconf-query -c xsettings -p /Net/ThemeName -s "Matcha-dark-sea";
        xfconf-query -c xsettings -p /Net/IconThemeName -s "Papirus-Dark";
       
        #zsh
        sudo apt install -y wget;
        sudo apt install -y curl;
        sudo apt install -y zsh;
        sudo apt install -y fonts-powerline;
        
        cd ~;
        git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh;
        wget -O .zshrc https://bit.ly/2ObQc5F;
        wget -O agnoster.zsh-theme https://bit.ly/3fkxnZY;
        cp -r agnoster.zsh-theme .oh-my-zsh/themes/;
        rm agnoster.zsh-theme;
        chsh -s /bin/zsh;
        
        sudo git clone https://github.com/robbyrussell/oh-my-zsh.git /root/.oh-my-zsh;
        sudo wget -O /root/.zshrc https://bit.ly/2DoGEBY;
        sudo wget -O /root/agnoster.zsh-theme https://bit.ly/2ObSxNS;
        sudo cp -r /root/agnoster.zsh-theme /root/.oh-my-zsh/themes;
        sudo rm /root/agnoster.zsh-theme;
        sudo xterm -e 'chsh -s /bin/zsh;' &
        xfce4-session-logout --fast --logout;
        
        ;;
        
    5)
        sudo apt remove -y --purge celluloid;
        sudo apt remove -y --purge drawing;
        sudo apt remove -y --purge xreader;
        sudo apt remove -y --purge simple-scan;
        sudo apt remove -y --purge gnote;
        sudo apt remove -y --purge hexchat;
        sudo apt remove -y --purge pix;
        sudo apt remove -y --purge rhythmbox;
        sudo apt remove -y --purge transmission-common;
        sudo apt remove -y --purge thunderbird;

        sudo wget --content-disposition https://bit.ly/2DwOoC9;
        sudo cp -r official-package-repositories.list /etc/apt/sources.list.d/;
        sudo rm official-package-repositories.list;
        sudo apt update;
        sudo apt upgrade -y;

        #brave browser dev
        sudo apt install -y apt-transport-https;
        sudo apt install -y curl;
        curl -s https://brave-browser-apt-dev.s3.brave.com/brave-core-nightly.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-prerelease.gpg add -;
        echo "deb [arch=amd64] https://brave-browser-apt-dev.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-dev.list;
        sudo apt update -y;
        sudo apt install brave-browser-dev -y;
        
        #discord
        wget -O discord.deb "https://discordapp.com/api/download?platform=linux&format=deb";
        sudo dpkg -i discord.deb;
        sudo apt install -f -y;
        rm discord.deb;
        
        sudo apt install -y git;
        sudo apt install -y curl;
        curl "https://raw.githubusercontent.com/bb010g/betterdiscordctl/master/betterdiscordctl" > betterdiscordctl;
        chmod +x betterdiscordctl;
        sudo mv betterdiscordctl /usr/local/bin/;
        nohup discord >& /dev/null &

        #gmusicbrowser
        git clone https://github.com/squentin/gmusicbrowser.git;
        cd gmusicbrowser/;
        sudo make install;
        cd ..;
        sudo rm -r gmusicbrowser/;
        
        #vlc
        sudo apt install -y vlc;

        #gnome softwarec
        sudo apt install -y gnome-software;

        #screenfetch
        sudo apt install -y screenfetch;
        
        #dislocker
        sudo apt install -y dislocker;
        
        killall firefox;
        sudo apt remove -y --purge firefox firefox-locale-en;
        betterdiscordctl install -s /usr/share;
        killall Discord;

        #ffmpeg
        sudo apt install -y ffmpeg;

        #resilio sync
        sudo wget -qO - https://linux-packages.resilio.com/resilio-sync/key.asc | sudo apt-key add -;
        sudo echo "deb http://linux-packages.resilio.com/resilio-sync/deb resilio-sync non-free" | sudo tee /etc/apt/sources.list.d/resilio-sync.list;
        sudo apt update -y
        sudo apt install -y resilio-sync;
        sudo service resilio-sync stop;
        sudo systemctl disable resilio-sync;
        wget https://gitlab.com/Proteus4/debian-install-scripts-4/-/raw/master/resilio-sync.service;
        sudo mv resilio-sync.service /usr/lib/systemd/user/;
        systemctl --user enable resilio-sync;
        systemctl --user start resilio-sync;

        #vscodium
        wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | sudo apt-key add -;
        echo 'deb https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list;
        sudo apt update -y;
        sudo apt install -y codium;

        #signal
        sudo apt install -y curl;
        curl -s https://updates.signal.org/desktop/apt/keys.asc | sudo apt-key add -;
        echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list;
        sudo apt update -y;
        sudo apt install -y signal-desktop;

        #teamviewer
        wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb;
        sudo apt install -y ./teamviewer_amd64.deb;
        rm teamviewer_amd64.deb;

        #audacity
        sudo apt install -y audacity;
        sudo apt install -y steam;

        #chicago 95
        sudo apt install -y git;
        git clone https://github.com/grassmunk/Chicago95.git;
        mkdir -p ~/.themes;
        mkdir -p ~/.icons;
        cp -r Chicago95/Theme/Chicago95 ~/.themes;
        cp -r Chicago95/Icons/* ~/.icons;
        sudo rm -r Chicago95;

        #arc-theme
        sudo apt install -y arc-theme

        #papirus icon theme
        sudo apt install -y papirus-icon-theme;
        
        sudo apt --fix-broken install -y;
        sudo apt autoremove -y;
        
        #tlp
        sudo add-apt-repository -y ppa:linrunner/tlp;
        sudo apt install -y tlp;

        #gmusicbrowser config
        wget --content-disposition https://bit.ly/2ZVFp4U;
        unzip -o gmusicbrowser.zip;
        cp -r gmusicbrowser ~/.config/;
        rm gmusicbrowser.zip;
        rm -r gmusicbrowser;
        
        #vlc config
        wget --content-disposition https://bit.ly/2Oe13Mf;
        unzip -o vlc.zip;
        cp -r vlc ~/.config/;
        rm -r vlc.zip;
        
        #xfce4 config
        rm -r ~/.config/xfce4;
        wget --content-disposition https://bit.ly/2ARU8FE;
        unzip -o xfce4.zip;
        cp -r xfce4 ~/.config/;
        rm -r xfce4.zip;
        
        #xfce4 panel config
        sudo apt install -y xfpanel-switch;
        wget --content-disposition https://bit.ly/3iQJWhM;
        xfce4-panel-profiles load mint-panel;
        rm mint-panel;
        
        #Matcha theme
        sudo apt install -y gtk2-engines-murrine;
        sudo apt install -y gtk2-engines-pixbuf;
        sudo apt install -y git;
        git clone https://github.com/vinceliuice/matcha.git;
        cd matcha;
        ./install.sh;
        cd ..;
        sudo rm -r matcha;
        
        #Papirus + Matcha Dark Sea config
        xfconf-query -c xfwm4 -p /general/theme -s "Matcha-dark-sea";
        xfconf-query -c xsettings -p /Net/ThemeName -s "Matcha-dark-sea";
        xfconf-query -c xsettings -p /Net/IconThemeName -s "Papirus-Dark";
        
        #encfs
        sudo apt install -y encfs;
        
        #zsh
        sudo apt install -y wget;
        sudo apt install -y curl;
        sudo apt install -y zsh;
        sudo apt install -y fonts-powerline;
        
        cd ~;
        git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh;
        wget -O .zshrc https://bit.ly/2ObQc5F;
        wget -O agnoster.zsh-theme https://bit.ly/3fkxnZY;
        cp -r agnoster.zsh-theme .oh-my-zsh/themes/;
        rm agnoster.zsh-theme;
        chsh -s /bin/zsh;
        
        sudo git clone https://github.com/robbyrussell/oh-my-zsh.git /root/.oh-my-zsh;
        sudo wget -O /root/.zshrc https://bit.ly/2DoGEBY;
        sudo wget -O /root/agnoster.zsh-theme https://bit.ly/2ObSxNS;
        sudo cp -r /root/agnoster.zsh-theme /root/.oh-my-zsh/themes;
        sudo rm /root/agnoster.zsh-theme;
        sudo xterm -e 'chsh -s /bin/zsh;' &
        rm proteus.sh;
        xfce4-session-logout --fast --logout;

        ;;
        
    99)
        break;
        ;;

    *)
        echo "Invalid input. Try again."
        ;;
    esac
done
